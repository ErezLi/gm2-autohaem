<p><b>Overview</b></p>
<p>We investigated the environmental factors that affect blood smears produced by the Autohaem Smear+, specifically focusing on temperature and relative humidity. First, the blood smears were produced in our desired environment and then they were fixed, stained and imaged using the Nikon Ti-E microscope.
</p>
<p>
    The images were then processed by the smear analysis pipeline which considers the red blood cell, RBC, count per field of view, the co-ordinate of each RBC, the number of neighbours each RBC has and how circular each RBC is. 
</p>
<p>
    For our investigation, we decided to take 2 smears for every environmental condition and imaged 20 fields of view per smear. The smear analysis pipeline averages over the 20 fields of view and outputs:
</p>
<p>
    <ul>
        <li>The median number of RBCs per field of view</li>
        <li>Interquartile range of RBCs per field of view</li>
        <li>•	Index of aggregation</li>
        <li>•	Mean count of adjacent neighbours per RBC</li>
        <li>•	RBC eccentricity</li>
    </ul>
</p>





<p><b>Smearing performance</b></p>
<p>
    The range of temperatures tested was 25⁰C to 40⁰C in 5⁰C intervals. Whilst the range in relative humidity was 40% to 70% in 10% intervals. Due to limitations in our experimental design, the conditions that could not be tested were 30⁰C at 70% humidity and 40⁰C at 60% and 70% humidity. 
</p>
<p>
    We also noticed anomalous results on our second day of experiments because all the environmental conditions that were tested, caused little or no RBC to be seen in each smear under the microscope. This was due to an experimental error that was carried throughout the day which is explained in more detail in the key findings report. These results should be discarded from our analysis and the conditions that gave the anomalies were: 30⁰C at 50% and 60% humidity, 35⁰C at 60% and 70% humidity, 40⁰C at 40% and 50%. 
</p>
<p>
    Our results are displayed in a heatmap for each of the five outputs from the smear analysis pipeline.  
</p>


<figure>
    <img src="/gm2-autohaem/assets/heatmap_median.jpg" alt="Median RBC count per field of view for each environmental condition tested." width="300">
    <figcaption>Figure 1: Median RBC count per field of view for each environmental condition tested.</figcaption>
</figure>
<p>
    From figure 1, we can see the 6 environmental conditions that have anomalous results as they have either 0 or 1 median RBC count per field of view
</p>
<p>
    Figure 1 indicates the median number of RBCs reduces as the temperature increases. This is expected because at higher temperatures, the viscosity of blood decreases which causes the blood smear to be thinner and there is less aggregation. At 25⁰C, the difference in the median count appears to be insignificant despite a small rise at 70% humidity. More datapoints would be required to confidently determine if there is a relationship between humidity and median RBC count per field of view.
</p>
<p>
    In the context of detecting malaria, a doctor will have to look at the blood smears through a microscope at different fields of view. Therefore, the median count value should not be too high as the image will be crowded so detecting the parasite may be harder. At the same time, if the count is too low, we would have to observe more fields of view which can slow down the time taken to diagnose someone with malaria.  
</p>


<figure>
    <img src="/gm2-autohaem/assets/heatmap_iqr.jpg" alt="Figure 2: Interquartile range per field of view for each environmental condition tested." width="300">
    <figcaption>Figure 2: Interquartile range per field of view for each environmental condition tested.</figcaption>
</figure>

<p>
    Having consistent smears can reduce the number of fields of view examined by a doctor therefore, it would be ideal for the interquartile range of RBC count per field of view to be low. At 25⁰C, we can clearly see a gradual decrease in the interquartile range as the relative humidity increases. This perhaps suggests that the blood smear is too thick at 40% and as it becomes thinner it improves the consistency. This is also supported by the fact that the interquartile range falls sharply at higher temperatures for a relative humidity of 40%. 
</p>


<figure>
    <img src="/gm2-autohaem/assets/heatmap_aggregation.jpg" alt="Figure 3: Mean index aggregation" width="300">
    <figcaption>Figure 3: Mean index aggregation</figcaption>
</figure>


<p>
    The mean index of aggregation is a measure of clustering. The ideal value is 2.15 which occurs when RBC are spaced in a regular pattern which is not very realistic.  The heatmap in figure 3 shows each environmental condition produces values close to 1 which means the spatial pattern is random. This also suggests that temperature and humidity do not have an impact on the mean index of aggregation. 
</p>


<figure>
    <img src="/gm2-autohaem/assets/heatmap_neighbours.jpg" alt="Figure 4: Mean adjacent neighbours per RBC." width="300">
    <figcaption>Figure 4: Mean adjacent neighbours per RBC.</figcaption>
</figure>


<p>
    Figure 4 suggests that as the temperature increases, the mean adjacent neighbours decrease. However, we noticed earlier that the median count of RBC decreases for higher temperatures so intuitively the number of adjacent neighbours should also decrease. Looking at 25⁰C, the results suggest higher humidity would be better for the smears. This is because a smaller number in the mean adjacent neighbours per RBC means the RBC are less clustered which makes detecting parasites much easier.
</p>

<figure>
    <img src="/gm2-autohaem/assets/heatmap_eccentricity.jpg" alt="Figure 5: Mean eccentricity per RBC" width="300">
    <figcaption>Figure 5: Mean eccentricity per RBC</figcaption>
</figure>


<p>
    Eccentricity is a measure of how circular the RBC are and a value of zero means it is a perfect circle. Ignoring the anomalies, this heatmap shows the RBC’s eccentricity is very similar for each condition. This suggests there is no correlation between the environmental condition and the eccentricity. 
</p>

<p>
    From our results, we can observe that humidity and temperature do affect the performance of smears. These environmental factors have a particular impact on the median count of RBC per field of view, interquartile range of RBC per field of view and mean adjacent neighbours per RBC. 
</p>

<p>
    However, experiments should be conducted for the environmental conditions that gave anomalous results as well as doing more repeats. This would enable us to be more confident about our observations and potentially find the optimal temperature and humidity for blood smears. 
</p>