---
layout: page
title: About
permalink: /about/
---

This is the website for GM2 project - Technology for the Poorest Billions, autohaem team at the University of Cambridge.

[More Information about Autohaem](https://www.autohaem.org/)

[The Center for Global Equality](https://centreforglobalequality.org/)

## Team Members

Erez Li 

Henry Dai 

Jathavan Thevarajah 

Joshua Abu 

## Special thanks to:

[Samuel McDermott](https://www.bss.phy.cam.ac.uk/directory/sjm263)

[Alexandre Kabla](http://www.eng.cam.ac.uk/profiles/ajk61)

[Lara Allen](https://www.cfse.cam.ac.uk/directory/lara_allen)

